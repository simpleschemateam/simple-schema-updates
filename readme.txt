-------------------------------------------------------
    Thank you for downloading Simple Schema!
-------------------------------------------------------

This Plugin provided by Simple Schema

Plugin Name:    Simple Schema
Plugin URI: simpleschema.com
Author: Simple Schema
Author URI: https://simpleschema.com

The most complete Semantic HTML Plugins for WordPress. 
Includes: Person, Product, Event, Organization, Movie, Book, and Review markup, 
and subtypes (Organization Type, Event Type).

-------------------------------------------------------
    Simple Schema Features:
-------------------------------------------------------

Promo Paragraf: Simple Schema is the most complete Semantic HTML Plugin available for WordPress. 
Search Engines use semantic markup to rank and display your content appropriately. 
Our plugin includes: Person, Product, Event, Organization, Movie, Book, and Review. 
Assign schemas per page or post; select where they display (Before Content, After Content, Hidden); even Preview them before saving. 
Simple Schema makes Semantic SEO as easy as selecting a few simple options and filling-in-the-blanks.